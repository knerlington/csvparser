package helpers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import models.Record;

/*DBHelper
 *Simplifies my use of a SQLite database .
 *Opens a connection and creates a db from the final CREATE_TABLE statement when instantiated.
 *Opens a connection to the same db when it's time to insert new data or retrieve the result from a query.*/
public class DBHelper {
	private static final String TABLE_NAME = "Entries", DATABASE_NAME = "test.db";
	private static final String CREATE_TABLE = "CREATE TABLE "+TABLE_NAME+" (_id INTEGER PRIMARY KEY, type STRING, measure STRING, value STRING)";
	
	public static String getTableName() {
		return TABLE_NAME;
	}
	public DBHelper() throws ClassNotFoundException{
		//load the sqlite-JDBC driver using the current class loader
	    Class.forName("org.sqlite.JDBC");
	    
		createDatabase();
		
	}
	public void insert(String query){
		//create a database connection
		Connection connection = null;
		try{
			connection = DriverManager.getConnection(String.format("jdbc:sqlite:%s",DATABASE_NAME));
			Statement statement = connection.createStatement();
			statement.executeUpdate(query);
		}catch(SQLException e){ System.err.println(e.getMessage());}
		finally{
			try{
				if(connection != null)
					connection.close();
			}catch(SQLException e){
				System.out.println(e.getMessage());
			}

		}
	}
	public void createDatabase(){
		//create a database connection
		Connection connection = null;
		try{
			connection = DriverManager.getConnection(String.format("jdbc:sqlite:%s",DATABASE_NAME));
			Statement statement = connection.createStatement();
			statement.setQueryTimeout(30);  // set timeout to 30 sec.
			statement.executeUpdate("DROP TABLE IF EXISTS "+DBHelper.TABLE_NAME);
			statement.executeUpdate(CREATE_TABLE);
		}catch(SQLException e){ System.err.println(e.getMessage());}
		finally{
			try{
				if(connection != null)
					connection.close();
			}catch(SQLException e){
				System.out.println(e.getMessage());
			}
			
		}

        

	}
	public ArrayList<Record> query(ArrayList<String> processTypes, ArrayList<String> measureTypes){
				//create a database connection
				Connection connection = null;
				ResultSet results = null;
				ArrayList<Record> records = new ArrayList<Record>();
				try{
					connection = DriverManager.getConnection(String.format("jdbc:sqlite:%s",DATABASE_NAME));
					Statement statement = connection.createStatement();
					
					/*loop through each process type
					 * from each type create a Record*/
					for(String s:processTypes){
						
						//String query = String.format("SELECT type, measure, avg(value) FROM %s WHERE type='%s' AND value!='ERROR'", DBHelper.getTableName(),s);
						Record record = null;
						String processQuery = String.format("SELECT type, measure FROM %s WHERE type='%s'", DBHelper.getTableName(),s);
						results = statement.executeQuery(processQuery);
						while(results.next()){
							record = new Record(results.getString("type"));
						}
						//loop through all measures WITHOUT ERROR for current process
						for(String m:measureTypes){
							String measureQuery = String.format("SELECT type, measure, avg(value) AS avg FROM %s WHERE type='%s' AND value!='ERROR' AND measure='%s'", DBHelper.getTableName(),s,m);
							results = statement.executeQuery(measureQuery);
							while(results.next()){
								
								//add each measure set to current record
								record.getMeasureSet().put(m, results.getFloat("avg"));
							}
						}
						//loop through all measures with ERROR for current process
						for(String m:measureTypes){
							String measureQuery = String.format("SELECT type, measure, count(value) AS count FROM %s WHERE type='%s' AND value='ERROR' AND measure='%s'", DBHelper.getTableName(),s,m);
							results = statement.executeQuery(measureQuery);
							while(results.next()){
								
								//add each measure set to current record
								record.getErrorSet().put(m, results.getInt("count"));
							}
						}
						records.add(record);
						
						
						
						
						
					}
					
				}catch(SQLException e){
					System.out.println(e.getMessage());
				}finally{
					try{
						if(connection != null)
							connection.close();
					}catch(SQLException e){
						System.out.println(e.getMessage());
					}
					
				}
		
		return records;
	}

}
