package models;

import java.util.HashMap;
import java.util.Map;

public class Record {
	public String type; //type of process
	private Map<String,Float> measureSet; //set with each avg measure-value from db
	private Map<String,Integer> errorSet; //set with each avg measure-error count from db

	public Map<String, Float> getMeasureSet() {
		return measureSet;
	}

	public Map<String, Integer> getErrorSet() {
		return errorSet;
	}

	public String getType() {
		return type;
	}

	public Record(String type) {
		this.type = type;
		this.measureSet = new HashMap<String, Float>();
		this.errorSet = new HashMap<String, Integer>();
	}

}
