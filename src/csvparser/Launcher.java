package csvparser;


import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;


import helpers.DBHelper;
import models.Record;

/*Launcher
 * Entry point of the program.
 * Creates a SQL database used to handle each line in a csv file.
 * Reads a csv file line by line, splits the lines from a separator, saves the columns in an array.
 * From the array the columns are used to create a SQL record for each line.
 * The database is then used to sort and calculate the average for each measure type.
 * The measure type and it's value are then saved as key-value pairs in a Record object.*/
public class Launcher {
	private static final String filepath = "csv/test_data_java_exercise.csv";
	private static ArrayList<String> processTypes, measureTypes;
	private static DBHelper db;
	private static ArrayList<Record> records; //records saved from sqlite queries
	
	public static void main(String[] args) throws ClassNotFoundException {
		 
		//init
		processTypes = new ArrayList<String>();
		measureTypes = new ArrayList<String>();
		records = new ArrayList<Record>();
		//create database
		db = new DBHelper();
		
		/*create buffered reader capable of counting lines*/
		LineNumberReader reader = null;
		String line = ""; //line in csv file
		String separator = ";"; //line separator
		
		
		try{
			reader = new LineNumberReader(new FileReader(filepath));
			while((line = reader.readLine()) != null){
				
				if(reader.getLineNumber()> 1){
					//skip first line since it contains only headers
					String[] columns = line.split(separator);
					String t = "type", m = "measure",v="value";
					//create db record from line
					String query = String.format("INSERT INTO %s (%s, %s, %s) VALUES('%s', '%s', '%s')", DBHelper.getTableName(),t,m,v,columns[0],columns[1],columns[2]);
					db.insert(query);
					
					//save process and measure types
					if(!processTypes.contains(columns[0])){
						//process unknown, add to list
						processTypes.add(columns[0]);
					}
					if(!measureTypes.contains(columns[1])){
						//measure type unknown, add to list
						measureTypes.add(columns[1]);
					}
				}
			}
			//query db and create Record
			queryDatabase();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			
			printResults();
			
			if(reader != null){
				try{

					reader.close();
				}catch(IOException e){
					
				}
			}
			
		}
	}
	
	private static void printResults(){
		System.out.println(":::::::::RESULTS:::::::::");
		//sort errors for each record
		for(Record r:records){
			List<Integer> errors = new ArrayList<Integer>(r.getErrorSet().values());
			Collections.sort(errors);
			StringBuffer line = new StringBuffer();
			line.append(r.type);
			//beginning with the highest error value print corresponding column for each row first
			for(int i = errors.size()-1;i > 0;i--){
				String spacer = "---";
				line.append(spacer);
				for(Map.Entry<String, Integer> map:r.getErrorSet().entrySet()){
					
					if(map.getValue().equals(errors.get(i))){
						line.append("Errors ");
						line.append(map.getKey());
						line.append(": ");
						line.append(map.getValue());
						for(Map.Entry<String, Float> measureMap:r.getMeasureSet().entrySet()){
							if(measureMap.getKey().equals(map.getKey())){
								line.append(spacer);
								line.append("AVG ");
								line.append(measureMap.getKey());
								line.append("-");
								line.append(measureMap.getValue());
							}
						}
					}
				}
				
			}
			System.out.println(line);
		}
		
		
		
	}
	private static void queryDatabase(){
		records = db.query(processTypes, measureTypes);
		
	}
}
